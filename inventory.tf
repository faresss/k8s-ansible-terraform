data  "template_file" "k8s" {
    template = "${file("./templates/k8s.tpl")}"
    vars = {
        k8s_master = "${join("\n", data.master_public_ip.test.*.ip_address, " node_ip=${data.master_private_ip.test.*.ip_address}"," node_user=${var.vm_user}")}"
        k8s_workers = "${join("\n", data.workers_public_ip.test.*.ip_address, " node_ip=${data.workers_private_ip.test.*.ip_address}"," node_user=${var.vm_user}")}"
    }
}

resource "local_file" "k8s_file" {
  content  = "${data.template_file.k8s.rendered}"
  filename = "./inventory/k8s-host"
}