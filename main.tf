terraform {

   required_version = ">=0.12"

   required_providers {
     azurerm = {
       source = "hashicorp/azurerm"
       version = "~>2.0"
     }
   }
 }

variable "vm_user" {
    default = "azureuser"
  
}

 variable "ssh_public_key" {
    default = "~/.ssh/id_rsa.pub"
  
}

 variable "ssh_private_key" {
    default = "~/.ssh/id_rsa"
  
}

 provider "azurerm" {
   features {}
 }

 resource "azurerm_resource_group" "k8sCluster" {
   name     = "k8sCluster"
   location = "Switzerland North"
 }

 resource "azurerm_virtual_network" "k8sCluster" {
   name                = "acctvn"
   address_space       = ["10.0.0.0/16"]
   location            = azurerm_resource_group.k8sCluster.location
   resource_group_name = azurerm_resource_group.k8sCluster.name
 }

 resource "azurerm_subnet" "k8sCluster" {
   name                 = "acctsub"
   resource_group_name  = azurerm_resource_group.k8sCluster.name
   virtual_network_name = azurerm_virtual_network.k8sCluster.name
   address_prefixes     = ["10.0.2.0/24"]
 }

resource "azurerm_public_ip" "k8sCluster" {
  count               = 3
  name                = "public_ip_${count.index}"
  location            = azurerm_resource_group.k8sCluster.location
  resource_group_name = azurerm_resource_group.k8sCluster.name
  allocation_method   = "Dynamic"
}

 resource "azurerm_network_interface" "k8sCluster" {
   count               = 3
   name                = "acctni${count.index}"
   location            = azurerm_resource_group.k8sCluster.location
   resource_group_name = azurerm_resource_group.k8sCluster.name

   ip_configuration {
     name                          = "testConfiguration"
     subnet_id                     = azurerm_subnet.k8sCluster.id
     private_ip_address_allocation = "dynamic"
     public_ip_address_id          = element(azurerm_public_ip.k8sCluster.*.id, count.index)

   }
 }

 resource "azurerm_managed_disk" "k8sCluster" {
   count                = 3
   name                 = "datadisk_existing_${count.index}"
   location             = azurerm_resource_group.k8sCluster.location
   resource_group_name  = azurerm_resource_group.k8sCluster.name
   storage_account_type = "Standard_LRS"
   create_option        = "Empty"
   disk_size_gb         = "40"
 }

 resource "azurerm_availability_set" "avset" {
   name                         = "avset"
   location                     = azurerm_resource_group.k8sCluster.location
   resource_group_name          = azurerm_resource_group.k8sCluster.name
   platform_fault_domain_count  = 2
   platform_update_domain_count = 2
   managed                      = true
 }



                  #### Worker nodes ####

 resource "azurerm_virtual_machine" "workers" {
   count                 = 2
   name                  = "worker_${count.index}"
   location              = azurerm_resource_group.k8sCluster.location
   availability_set_id   = azurerm_availability_set.avset.id
   resource_group_name   = azurerm_resource_group.k8sCluster.name
   network_interface_ids = [element(azurerm_network_interface.k8sCluster.*.id, count.index)]
   vm_size               = "Standard_DS1_v2"
   

   # Uncomment this line to delete the OS disk automatically when deleting the VM
   # delete_os_disk_on_termination = true

   # Uncomment this line to delete the data disks automatically when deleting the VM
   # delete_data_disks_on_termination = true

   storage_image_reference {
     publisher = "Canonical"
     offer     = "UbuntuServer"
     sku       = "18.04-LTS"
     version   = "latest"
   }

   storage_os_disk {
     name              = "myosdisk${count.index}"
     caching           = "ReadWrite"
     create_option     = "FromImage"
     managed_disk_type = "Standard_LRS"
   }


   storage_data_disk {
     name            = element(azurerm_managed_disk.k8sCluster.*.name, count.index)
     managed_disk_id = element(azurerm_managed_disk.k8sCluster.*.id, count.index)
     create_option   = "Attach"
     lun             = 1
     disk_size_gb    = element(azurerm_managed_disk.k8sCluster.*.disk_size_gb, count.index)
   }


   os_profile {
     computer_name  = "hostname"
     admin_username = "${var.vm_user}"
   }

   os_profile_linux_config {
     disable_password_authentication = true
     ssh_keys {
          path     = "/home/${var.vm_user}/.ssh/authorized_keys"
          key_data = "${file("/home/fares/.ssh/id_rsa.pub")}"    

   }
   }

   tags = {
     environment = "staging"
   }


  #provisioner "local-exec" {command = "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -u ${var.vm_user} -i ./inventory/k8s-host --private-key ${var.ssh_private_key} deploy_worker.yml"}

 }

data "workers_private_ip" "test" {
  count               = 2
  name                = "${element(azurerm_network_interface.k8sCluster.*.private_ip_adress, count.index)}"
  resource_group_name = "${azurerm_resource_group.k8sCluster.name}"
}



data "workers_public_ip" "test" {
  count               = 2
  name                = "${element(azurerm_public_ip.k8sCluster.*.name, count.index)}"
  resource_group_name = "${azurerm_resource_group.k8sCluster.name}"
}


                       ### Master node ###

 resource "azurerm_virtual_machine" "master" {
   name                  = "master"
   location              = azurerm_resource_group.k8sCluster.location
   availability_set_id   = azurerm_availability_set.avset.id
   resource_group_name   = azurerm_resource_group.k8sCluster.name
   network_interface_ids = [element(azurerm_network_interface.k8sCluster.*.id, 2)]
   vm_size               = "Standard_B2s"
   

   # Uncomment this line to delete the OS disk automatically when deleting the VM
   # delete_os_disk_on_termination = true

   # Uncomment this line to delete the data disks automatically when deleting the VM
   # delete_data_disks_on_termination = true

   storage_image_reference {
     publisher = "Canonical"
     offer     = "UbuntuServer"
     sku       = "18.04-LTS"
     version   = "latest"
   }

   storage_os_disk {
     name              = "myosdisk3"
     caching           = "ReadWrite"
     create_option     = "FromImage"
     managed_disk_type = "Standard_LRS"
   }


   storage_data_disk {
     name            = element(azurerm_managed_disk.k8sCluster.*.name, 2)
     managed_disk_id = element(azurerm_managed_disk.k8sCluster.*.id, 2)
     create_option   = "Attach"
     lun             = 1
     disk_size_gb    = element(azurerm_managed_disk.k8sCluster.*.disk_size_gb, 2)
   }


   os_profile {
     computer_name  = "hostname"
     admin_username = "${var.vm_user}"
   }

   os_profile_linux_config {
     disable_password_authentication = true
     ssh_keys {
          path     = "/home/${var.vm_user}/.ssh/authorized_keys"
          key_data = "${file("${var.ssh_public_key}")}"    

   }
   }


   #provisioner "local-exec" {command = "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -u ${var.vm_user} -i ./inventory/k8s-host --private-key ${var.ssh_private_key} deploy_master.yml"}

   tags = {
     environment = "staging"
   }
 }


 data "master_public_ip" "test" {
  name                = "${element(azurerm_public_ip.k8sCluster.*.name, 2)}"
  resource_group_name = "${azurerm_resource_group.k8sCluster.name}"
}

 data "master_private_ip" "test" {
  ip_address          = "${element(azurerm_network_interface.k8sCluster.*.private_ip_adress, 2)}"
  resource_group_name = "${azurerm_resource_group.k8sCluster.name}"
}


